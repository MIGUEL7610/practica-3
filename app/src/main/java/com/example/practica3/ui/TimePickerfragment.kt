package com.example.practica3.ui

import android.app.Dialog
import android.app.TimePickerDialog
import android.icu.text.DateFormat
import android.os.Bundle
import android.provider.ContactsContract
import android.text.method.TimeKeyListener
import android.widget.TimePicker
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import java.util.*

class TimePickerfragment:DialogFragment() {
    private var listener: TimePickerDialog.OnTimeSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar= Calendar.getInstance()
        val hour= calendar.get(Calendar.HOUR_OF_DAY)
        val minute=calendar.get(Calendar.MINUTE)

        return TimePickerDialog(activity, listener, hour, minute, android.text.format.DateFormat.is24HourFormat(activity))
    }
     companion object{
     fun newInstance(listener: TimePickerDialog.OnTimeSetListener): TimePickerfragment{
        val fragment = TimePickerfragment()

         fragment.listener = listener
         return fragment

     }
 }

}