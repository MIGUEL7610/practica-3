package com.example.practica3

import android.app.TimePickerDialog
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.practica3.ui.TimePickerfragment
import kotlinx.android.synthetic.main.activity_picker.*

class PickerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
    }
    fun ShowPictureDialogo(v: View){
       // val timePickerfragment = TimePickerfragment()
        val timePickerfragment = TimePickerfragment.newInstance(TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            pickertime.setText("${hourOfDay}:${minute}")
        })
        timePickerfragment.show(supportFragmentManager, "DatePicture")

    }
}